import numpy as np
from matplotlib import pyplot as plt

# problem dimension
from module_linreg import gradient_descent_linreg

m = 100

data_x = np.linspace(1.0, 10.0, m)[:, np.newaxis]
data_y = np.sin(data_x) + 0.1*np.power(data_x, 2) + 0.5*np.random.randn(m, 1)
data_x /= np.max(data_x)

#plt.plot(range(m), data_y)
#plt.show()

# absorb intercept  ( w_tilde = [w b] )  and augment accordingly  each data point ( x_tilde = [x 1] )
data_x = np.hstack((data_x, np.ones_like(data_x)))

order = np.random.permutation(len(data_x))
portion = 20

test_x = data_x[order[:portion], :].transpose()
test_y = data_y[order[:portion], :].transpose()

train_x = data_x[order[portion:], :].transpose()
train_y = data_y[order[portion:], :].transpose()

#print(train_x)
#print(train_y)

w_init = np.random.randn(data_x.shape[1])[:, np.newaxis]

tol = 0.0001
lr = 5
(loss_history, w_hat) = gradient_descent_linreg(w_init, lr, tol, train_x, train_y)

plt.plot(range(len(loss_history)), loss_history)
plt.show()
x = np.linspace(0,1, 1000)
y = w_hat[0]*x+w_hat[1]
plt.plot(x, y)
plt.scatter(train_x[0,:], train_y)
plt.show()
