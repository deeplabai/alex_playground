import numpy as np


def loss_linreg(w, x, y):
    return np.power(np.dot(w.T, x) - y, 2)


def grad_loss_linreg(w, x, y):
    return 2 * (np.dot(w.T, x) - y) * x


def gradient_descent_linreg(w_init, lr, tol,  x_mat, y):
    w_old = w_init
    w_new = 0

    grad_total = 1000

    all_losses = []
    while np.linalg.norm(grad_total) > tol:
        loss_total = 0
        grad_total = 0
        i = 0
        for x_i in x_mat.T:
            x_i.shape = (2, 1)
            grad_ = grad_loss_linreg(w_old, x_i, y[:,i])
            i = i + 1

            grad_total = grad_total + grad_
            grad_total = grad_total*(1/x_mat.shape[1])

        #print(grad_total)
        w_new = w_old - lr * grad_total
        for i in range(x_mat.shape[1]):
            x_i = x_mat[:,i]
            x_i.shape = (2,1)
            loss_total = loss_total + loss_linreg(w_new, x_i, y[:,i])
        w_old = w_new
        loss_total = loss_total*(1/x_mat.shape[1])

        all_losses.append(loss_total[0][0])
        print(loss_total)
        #print(w_new)

    return (all_losses, w_new)





